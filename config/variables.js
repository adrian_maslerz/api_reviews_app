//loading environment config
const env = process.env;

module.exports = {
	database: {
		host: env.DB_HOST || "localhost",
		port: env.DB_PORT || 27017,
		user: env.DB_USER || "",
		password: env.DB_PASSWORD || "",
		database: env.DB_DATABASE || ""
	},
	folders: {
		views: __dirname + "/../views",
		uploads: __dirname + "/../public/uploads",
		public: __dirname + "/../public",
	},
	paths: {
		folders: {
			uploads: "/uploads"
		}
	}
}