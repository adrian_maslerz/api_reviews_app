//
const AppRouter = require('../app/services/core/AppRouter');

//controllers
const ETLController = require('../app/controllers/api/ETLController');
const ProductsController = require('../app/controllers/api/ProductsController');
const ReviewsController = require('../app/controllers/api/ReviewsController');

//module.exports = router;
const path = "/api";
const routes = [

	//ETL process
	{
		path: "/etl", controller: ETLController, children: [
			{ path: "/", method: "post", function: "etl" },
			{ path: "/extract", method: "post", function: "extract" },
			{ path: "/transform", method: "post", function: "transform" },
			{ path: "/load", method: "post", function: "load" },
			{ path: "/purge", method: "delete", function: "clearDatabase" },
		]
	},

	//Products
    {
        path: "/products", controller: ProductsController, children: [
            { path: "/", method: "get", function: "getProducts" },
            { path: "/export", method: "get", function: "exportProducts" },
            { path: "/:id", method: "get", function: "getProduct" },
            { path: "/:id/export", method: "get", function: "exportProduct" },
            { path: "/:id", method: "put", function: "updateProduct" },

			//Reviews
            { path: "/:id/reviews", controller: ReviewsController, method: "get", function: "getProductReviews" },
        ]
    },

    //Reviews
    {
        path: "/reviews", controller: ReviewsController, children: [
            { path: "/:id", method: "get", function: "getReview" },
            { path: "/:id/export", method: "get", function: "exportReview" },
            { path: "/:id", method: "put", function: "updateReview" },
        ]
    }
];

module.exports.load = function(app)
{
	//create routes
	let appRouter = new AppRouter();
	appRouter.create(routes);

	//documentation
	appRouter.registerDocumentation(path);

	app.use(path, appRouter.router);
}


