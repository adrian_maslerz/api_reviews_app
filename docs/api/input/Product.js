
/**
 * @api {get} /products Products list
 * @apiSampleRequest /products
 * @apiVersion 0.0.1
 * @apiName Products list
 * @apiGroup Product
 * 
 * @apiParam {Number=10} results Pagination page result.
 * @apiParam {Number=1} page Pagination page page number.
 *
 * @apiSuccess (Success 200){Object[]} results Pagination results objects array.
 * @apiSuccess (Success 200){Number} pages Pagination total pages.
 * @apiSuccess (Success 200){Number} total Pagination total items.
 * @apiSuccess (Success 200){String} results._id Product id.
 * @apiSuccess (Success 200){String} results.name Product name.
 * @apiSuccess (Success 200){Number} results.created Product loaded date as timestamp in milliseconds.
 * @apiSuccess (Success 200){String} results.image Product image path.
 * @apiSuccess (Success 200){String} results.reviews_count Product reviews count.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "results": [
 *              {
 *                  "_id": "5c117e648d434b179088d58f",
 *                  "name": "3mk ARC 3D High Grip do Samsung Galaxy S8",
 *                  "created": 1544650345006,
 *                  "image": "http://localhost:8000/uploads/external/15446503410850.02400292684722011.jpg",
 *                  "reviews_count": 14
 *              }
 *          ],
 *          "pages": 40,
 *          "total": 394
 *     }
 */

/**
 * @api {get} /products/export Products with reviews as CSV
 * @apiSampleRequest /products/export
 * @apiVersion 0.0.1
 * @apiName Products with reviews as CSV
 * @apiGroup Product
 */

/**
 * @api {get} /products/:id Product details
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Product details
 * @apiGroup Product
 *
 * @apiParam {String} id Product id.
 *
 * @apiSuccess (Success 200){String} _id Product id.
 * @apiSuccess (Success 200){String} name Product name.
 * @apiSuccess (Success 200){String} link Product source link.
 * @apiSuccess (Success 200){Number} created Product loaded date as timestamp in milliseconds.
 * @apiSuccess (Success 200){String} image Product image path.
 * @apiSuccess (Success 200){Object[]} attributes Attributes object array.
 * @apiSuccess (Success 200){String} attributes.name Attribute name.
 * @apiSuccess (Success 200){String[]} attributes.values Attribute values array.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "_id": "5c117e648d434b179088d58f",
 *          "name": "3mk ARC 3D High Grip do Samsung Galaxy S8",
 *          "link": "https://www.x-kom.pl/p/361066-folia-szklo-na-smartfon-3mk-arc-3d-high-grip-do-samsung-galaxy-s8.html"
 *          "created": 1544650345006,
 *          "image": "http://localhost:8000/uploads/external/15446503410850.02400292684722011.jpg",
 *          "attributes": [
 *              {
 *                  "name": "Typ",
 *                  "values": [
 *                      "Folia"
 *                  ]
 *              },
 *          ]
 *     }
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Product not found"
 *              }
 *          ]
 *      }
 */

/**
 * @api {get} /products/:id/export Product export as TXT
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Product export as TXT
 * @apiGroup Product
 *
 * @apiParam {String} id Product id.
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Product not found"
 *              }
 *          ]
 *      }
 */

/**
 * @api {put} /products/:id Product update
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Product update
 * @apiGroup Product
 *
 * @apiParam {String} id Product id.
 * @apiParam {String} [name] Product name.
 * @apiParam {File} [image] Product image. Max 5MB. Mime types: image/png, image/jpg, image/jpeg
 * @apiParam {String} [attributes] Attributes array string
 *

 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "status" : true,
 *     }
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Product not found"
 *              }
 *          ]
 *      }
 *
 * @apiError WrongParameters Wrong parameters.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "name",
 *                   "message": "Name is required"
 *              }
 *          ]
 *      }
 */