
/**
 * @api {get} /products/:id/reviews Reviews list
 * @apiSampleRequest /products/:id/reviews
 * @apiVersion 0.0.1
 * @apiName Reviews list
 * @apiGroup Review
 * 
 * @apiParam {Number=10} results Pagination page result.
 * @apiParam {Number=1} page Pagination page page number.
 * @apiParam {String} id Product id.
 *
 * @apiSuccess (Success 200){Object[]} results Pagination results objects array.
 * @apiSuccess (Success 200){Number} pages Pagination total pages.
 * @apiSuccess (Success 200){Number} total Pagination total items.
 * @apiSuccess (Success 200){String} results._id Review id.
 * @apiSuccess (Success 200){String} results.description Review description.
 * @apiSuccess (Success 200){Number} results.ups Review ups.
 * @apiSuccess (Success 200){Number} results.downs Review downs.
 * @apiSuccess (Success 200){Number} results.rate Review rate.
 * @apiSuccess (Success 200){Number} results.date Review posted date as timestamp in milliseconds.
 * @apiSuccess (Success 200){Number} results.created Review loaded date as timestamp in milliseconds.
 * @apiSuccess (Success 200){Object} results.user Review user object.
 * @apiSuccess (Success 200){String} results.user.name User name.

 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "results": [
 *              {
 *                  "_id": "5c117e648d434b179088d595",
 *                  "description": "Folia zdała egzamin celująco.",
 *                  "ups": 0,
 *                  "downs": 0,
 *                  "rate": 6,
 *                  "date": 1515246840000,
 *                  "created": 1544650363817,
 *                  "user": {
 *                      "name": "benek"
 *                  }
 *              }
 *          ],
 *          "pages": 40,
 *          "total": 394
 *     }
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Product not found"
 *              }
 *          ]
 *      }
 */

/**
 * @api {get} /reviews/:id Review details
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Review details
 * @apiGroup Review
 *
 * @apiParam {String} id Review id.
 *
 * @apiSuccess (Success 200){String} _id Review id.
 * @apiSuccess (Success 200){String} description Review description.
 * @apiSuccess (Success 200){Number} ups Review ups.
 * @apiSuccess (Success 200){Number} downs Review downs.
 * @apiSuccess (Success 200){Number} rate Review rate.
 * @apiSuccess (Success 200){Number} date Review posted date as timestamp in milliseconds.
 * @apiSuccess (Success 200){Number} created Review loaded date as timestamp in milliseconds.
 * @apiSuccess (Success 200){Object} user Review user object.
 * @apiSuccess (Success 200){String} user.name User name.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "_id": "5c117e648d434b179088d595",
 *          "description": "Folia zdała egzamin celująco.",
 *          "ups": 0,
 *          "downs": 0,
 *          "rate": 6,
 *          "date": 1515246840000,
 *          "created": 1544650363817,
 *          "user": {
 *              "name": "benek"
 *          }
 *     }
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "review",
 *                   "message": "Review not found"
 *              }
 *          ]
 *      }
 */

/**
 * @api {get} /reviews/:id/export Review export as TXT
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Review export as TXT
 * @apiGroup Review
 *
 * @apiParam {String} id Review id.
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "review",
 *                   "message": "Review not found"
 *              }
 *          ]
 *      }
 */

/**
 * @api {put} /reviews/:id Review update
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Review update
 * @apiGroup Review
 *
 * @apiParam {String} id Review id.
 * @apiParam {String} [description] Review description.
 * @apiParam {Number} [ups] Review ups. Min 0
 * @apiParam {Number} [downs] Review ups. Min 0
 * @apiParam {Number} [rate] Review ups. Min 1. Max 6.
 * @apiParam {Number} [date] Review publish date.
 * @apiParam {Number} [user] Review user name.
 *
 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "status" : true,
 *     }
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "review",
 *                   "message": "Review not found"
 *              }
 *          ]
 *      }
 *
 * @apiError WrongParameters Wrong parameters.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "description",
 *                   "message": "Description is required"
 *              }
 *          ]
 *      }
 */