
/**
 * @api {post} /etl Full ETL
 * @apiSampleRequest /etl
 * @apiVersion 0.0.1
 * @apiName Full etl
 * @apiGroup ETL
 *
 * @apiParam {String} search Search string. Min 4 characters

 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "status" : true,
 *     }
 *
 * @apiError NotAcceptable Wrong parameters.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "search",
 *                   "message": "Szukana fraza musi zawierać przynajmniej 4 znaki"
 *              }
 *          ]
 *      }
 *
 * @apiError NotAcceptable Conflict
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "etl",
 *                   "message": "Nie można użyć ponownie ETL w momencie przetwarzania"
 *              }
 *          ]
 *      }
 */

/**
 * @api {post} /etl/extract Extract process
 * @apiSampleRequest /etl/extract
 * @apiVersion 0.0.1
 * @apiName Extract process
 * @apiGroup ETL
 *
 * @apiParam {String} search Search string. Min 4 characters

 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "status" : true,
 *     }
 *
 * @apiError NotAcceptable Wrong parameters.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "search",
 *                   "message": "Szukana fraza musi zawierać przynajmniej 4 znaki"
 *              }
 *          ]
 *      }
 *
 * @apiError NotAcceptable Conflict
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "etl",
 *                   "message": "Nie można użyć ponownie ETL w momencie przetwarzania"
 *              }
 *          ]
 *      }
 */

/**
 * @api {post} /etl/transform Transform process
 * @apiSampleRequest /etl/transform
 * @apiVersion 0.0.1
 * @apiName Transform process
 * @apiGroup ETL

 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "status" : true,
 *     }
 *
 *
 * @apiError NotAcceptable Conflict
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "etl",
 *                   "message": "Nie można użyć ponownie ETL w momencie przetwarzania"
 *              }
 *          ]
 *      }
 */

/**
 * @api {post} /etl/load Load process
 * @apiSampleRequest /etl/load
 * @apiVersion 0.0.1
 * @apiName Load process
 * @apiGroup ETL

 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "status" : true,
 *     }
 *
 *
 * @apiError NotAcceptable Conflict
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "etl",
 *                   "message": "Nie można użyć ponownie ETL w momencie przetwarzania"
 *              }
 *          ]
 *      }
 */

/**
 * @api {delete} /etl/purge Clear database
 * @apiSampleRequest /etl/purge
 * @apiVersion 0.0.1
 * @apiName Clear database
 * @apiGroup ETL

 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "status" : true,
 *     }
 */