
//custom
const Utils = require('../../services/utilities');

//models
const { Review } = require("./../../models/Review");
const { Product } = require("./../../models/Product");

module.exports = class ReviewsController
{
	async getProductReviews(req, res)
    {
        const product = await Product.findById(req.params.id).exec().catch(error => console.log(error));

        //not found
        if(!product)
            return res.status(404).json(Utils.parseStringError("Product not found", "product"));

        const options = {
            select: {
                user: 1,
                description: 1,
                ups: 1,
                downs: 1,
                rate: 1,
                date: 1,
                created: 1,
            },
            sort: { date: -1 },
            lean: true,
            leanWithId: false
        };

        const query = { product: product._id };

        //pagination and parsing
        const paginated = await Utils.paginate(Review, { query: query, options: options }, req);
        paginated.results.map(review => Review.parse(review, req));

        res.json(paginated);
    }

    async getReview(req, res)
    {
        const review = await Review.findById(req.params.id,
            {
                user: 1,
                description: 1,
                ups: 1,
                downs: 1,
                rate: 1,
                date: 1,
                created: 1
            })
            .lean()
            .exec()
            .catch(error => console.log(error));

        //not found
        if(!review)
            return res.status(404).json(Utils.parseStringError("Review not found", "review"));

        res.json(Review.parse(review, req));
    }

    async updateReview(req, res)
    {
        const review = await Review.findById(req.params.id).exec().catch(error => console.log(error));

        //not found
        if(!review)
            return res.status(404).json(Utils.parseStringError("Review not found", "review"));

        review.set({
            user: req.body.user || review.user.name,
            description: req.body.description || review.description,
            ups: req.body.ups != undefined ? req.body.ups : review.ups,
            downs: req.body.downs != undefined ? req.body.downs : review.downs,
            rate: req.body.rate != undefined ? req.body.rate : review.rate,
            date: req.body.date != undefined ? req.body.date : review.date,
        });

        //saving review
        review
            .save()
            .then(() => res.json({ status: true}))
            .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
    }

    async exportReview(req, res)
    {
        const review = await Review.findById(req.params.id,
            {
                user: 1,
                description: 1,
                ups: 1,
                downs: 1,
                rate: 1,
                date: 1,
                created: 1
            })
            .lean()
            .exec()
            .catch(error => console.log(error));

        //not found
        if(!review)
            return res.status(404).json(Utils.parseStringError("Review not found", "review"));

        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.setHeader("Content-Disposition", 'attachment; filename=review.txt');

        res.send(JSON.stringify(review));
    }
}