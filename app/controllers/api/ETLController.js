
//custom
const Utils = require('../../services/utilities');
const FileHandler = require("./../../services/core/FileHandler");

//models
const { Product } = require("./../../models/Product");
const { Review } = require("./../../models/Review");

module.exports = class ETLController
{
	async etl(req, res)
    {
        //in processing
        if(req.app.etl.processing)
            return res.status(409).json(Utils.parseStringError("Nie można użyć ponownie ETL w momencie przetwarzania","etl"))

        //search
        const search =  req.body.search ? req.body.search.trim() : "";
        if(search.length < 4)
            return res.status(406).json(Utils.parseStringError("Szukana fraza musi zawierać przynajmniej 4 znaki","search"));

        //sending response
        res.json({ status: true });

        //FULL ETL PROCESSING
        await req.app.etl.extractData(search);
        req.app.etl.transformData();
        await req.app.etl.loadData();
    }

    async extract(req, res)
    {
        //in processing
        if(req.app.etl.processing)
            return res.status(409).json(Utils.parseStringError("Nie można użyć ponownie ETL w momencie przetwarzania","etl"))

        //search
        const search =  req.body.search ? req.body.search.trim() : "";
        if(search.length < 4)
            return res.status(406).json(Utils.parseStringError("Szukana fraza musi zawierać przynajmniej 4 znaki","search"));

        //sending response
        res.json({ status: true });

        //EXTRACT PROCESSING
        await req.app.etl.extractData(search);
    }

    async transform(req, res)
    {
        //in processing
        if(req.app.etl.processing)
            return res.status(409).json(Utils.parseStringError("Nie można użyć ponownie ETL w momencie przetwarzania","etl"))

        //extract fail
        if(!req.app.etl.extract)
            return res.status(409).json(Utils.parseStringError("Nie można użyć operacji transform bez wcześniejszego wywołania operacji extract","etl"))

        //sending response
        res.json({ status: true });

        //TRANSFORM PROCESSING
        req.app.etl.transformData();
    }

    async load(req, res)
    {
        //in processing
        if(req.app.etl.processing)
            return res.status(409).json(Utils.parseStringError("Nie można użyć ponownie ETL w momencie przetwarzania","etl"))

        //extract fail
        if(!req.app.etl.transform)
            return res.status(409).json(Utils.parseStringError("Nie można użyć operacji load bez wcześniejszego wywołania operacji transform","etl"))

        //sending response
        res.json({ status: true });

        //LOADING DATA
        await req.app.etl.loadData();
    }

    async clearDatabase(req, res)
    {
        //removing images
        const products = await Product.find().exec();
        products.forEach(product => {
            if(product.image)
                FileHandler.deleteFile(product.image);
        })

        //removing Products and Reviews
        await Product.remove({}).catch(error => console.log(error));
        await Review.remove({}).catch(error => console.log(error));

        //sending response
        res.json({ status: true });
    }
}