
//custom
const Utils = require('../../services/utilities');

//models
const { Product } = require("./../../models/Product");

//services
const FileHandler = require("./../../services/core/FileHandler");
const Json2csvParser = require('json2csv').Parser;

module.exports = class ProductsController
{
	async getProducts(req, res)
    {
        //params
        const search = req.query.search || "";
        const pattern = new RegExp("^.*" + search + ".*$");

        const pipeline = [
            {
                $match: {
                    name: { $regex: pattern, $options: "i" }
                }
            },
            {
                $project: {
                    name: 1,
                    image: 1,
                    reviews_count: { $size: "$reviews" },
                    created: 1
                }
            },
        ];

        //preparing bounds dates
        let fromDate = !isNaN(Date.parse(req.query["from_date"]))? new Date(req.query["from_date"]) : !isNaN(parseInt(req.query["from_date"]))? new Date(parseInt(req.query["from_date"])) : false;
        let toDate = !isNaN(Date.parse(req.query["to_date"])) ? new Date(req.query["to_date"]) : !isNaN(parseInt(req.query["to_date"]))? new Date(parseInt(req.query["to_date"])) : false;
        if(fromDate && toDate && fromDate.getTime() > toDate.getTime())
        {
            const temp = toDate;
            toDate = fromDate;
            fromDate = temp;
        }

        //filter by created
        if(fromDate)
        {
            fromDate.setHours(0, 0, 0, 0);
            pipeline[0].$match["$and"] = [ { created: { $gte: fromDate } } ];
        }
        if(toDate)
        {
            toDate.setHours(23, 59, 59, 999);
            if(pipeline[0].$match["$and"])
                pipeline[0].$match["$and"].push({ created: { $lte: toDate } })
            else
                pipeline[0].$match["$and"] = [ { created: { $lte: toDate } } ];
        }

        //preparing reviews bounds
        let fromReviews = !isNaN(parseInt(req.query["from_reviews"])) ? parseInt(req.query["from_reviews"]) : false;
        let toReviews = !isNaN(parseInt(req.query["to_reviews"])) ? parseInt(req.query["to_reviews"]) : false;
        if(typeof fromReviews == "number" && typeof toReviews == "number" && fromReviews > toReviews)
        {
            const temp = toReviews;
            toReviews = fromReviews;
            fromReviews = temp;
        }

        //filter by reviews
        if(fromReviews)
        {
            pipeline.push({ $match: { $and: [ { reviews_count: { $gte: fromReviews } } ] } });
        }
        if(toReviews)
        {
            if(pipeline[pipeline.length - 1].$match && pipeline[pipeline.length - 1].$match["$and"])
                pipeline[pipeline.length - 1].$match["$and"].push({ reviews_count: { $lte: toReviews } })
            else
                pipeline.push({ $match: { $and: [ { reviews_count: { $lte: toReviews } } ] } });
        }

        //sorts
        const options = { sortBy: { name: 1 }};

        //pagination and parsing
        const paginated = await Utils.paginate(Product, { query: Product.aggregate(pipeline), options: options }, req, true);
        paginated.results.map(product => Product.parse(product, req));

        res.json(paginated);
    }

    async getProduct(req, res)
    {
        const product = await Product.findById(req.params.id,
            {
                name: 1,
                image: 1,
                link: 1,
                "attributes.name": 1,
                "attributes.values": 1,
                created: 1,
            })
            .lean()
            .exec()
            .catch(error => console.log(error));

        //not found
        if(!product)
            return res.status(404).json(Utils.parseStringError("Product not found", "product"));

        res.json(Product.parse(product, req));
    }

    async updateProduct(req, res)
    {
        const product = await Product.findById(req.params.id).exec().catch(error => console.log(error));

        //not found
        if(!product)
            return res.status(404).json(Utils.parseStringError("Product not found", "product"));

        //image upload
        const handler = new FileHandler(req, res);
        const filePath = await handler.handleSingleUpload("image", "products/" + product._id, {
            allowedMimeTypes: [ "image/jpg", "image/jpeg", "image/png" ],
            fileToRemove: product.image
        });

        //parsing attributes
        let attributes = null;
        try { attributes = JSON.parse(req.body["attributes"]) } catch (error) {}

        product.set({
            name: req.body.name || product.name,
            image: !filePath && req.body.image == "" ? null : (filePath || product.image),
            attributes: attributes || product.attributes
        });

        //saving product
        product
            .save()
            .then(() => res.json({ status: true}))
            .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
    }

    async exportProducts(req, res)
    {
        //getting data
        const pipeline = [
            {
                $lookup: {
                    from: "reviews",
                    as: "reviews",
                    let: { productId: "$_id" },
                    pipeline: [
                        {
                            $match: { $expr: { $eq: ["$product", "$$productId"] } }
                        },
                        {
                            $project: {
                                user: 1,
                                description: 1,
                                ups: 1,
                                downs: 1,
                                rate: 1,
                                date: 1,
                                created: 1,
                            }
                        }
                    ]
                }
            },
            {
                $project: {
                    name: 1,
                    image: 1,
                    reviews: 1,
                    link: 1,
                    attributes: 1,
                    created: 1
                }
            },
            { $limit: 100}
        ];

        const data = await Product.aggregate(pipeline).exec().catch(error => console.log(error));

        //preparing csv
        const options = {
            withBOM: true
        }

        const fields = [
            //basic
            { label: "Product id", value: "_id" }, { label: "Product name", value: "name"}, { label: "Product link", value: "link"},
            { label: "Product loaded date", value: "created"}, { label: "Image link", value: "image"},

            //attributes
            { label: "Attribute name", value: "attributes.name"}, { label: "Attribute value", value: "attributes.values"},

            //reviews
            { label: "Review id", value: "reviews._id"}, { label: "Review description", value: "reviews.description"}, { label: "Review author", value: "reviews.user.name"},
            { label: "Review ups", value: "reviews.ups"}, { label: "Review downs", value: "reviews.downs"}, { label: "Review rate", value: "reviews.rate"},
            { label: "Review created date", value: "reviews.date"}, { label: "Review loaded date", value: "reviews.created"},

        ];

        options["unwind"] = ["reviews", "attributes", "attributes.values"];

        const parser = new Json2csvParser({ fields, ...options });
        const csv = parser.parse(data);

        console.log("aa")
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/csv');
        res.setHeader("Content-Disposition", 'attachment; filename=database.csv');

        res.send(csv);
    }

    async exportProduct(req, res)
    {
        const product = await Product.findById(req.params.id,
            {
                name: 1,
                image: 1,
                link: 1,
                "attributes.name": 1,
                "attributes.values": 1,
                created: 1,
            })
            .lean()
            .exec()
            .catch(error => console.log(error));

        //not found
        if(!product)
            return res.status(404).json(Utils.parseStringError("Product not found", "product"));

        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.setHeader("Content-Disposition", 'attachment; filename=product.txt');

        res.send(JSON.stringify(product));
    }
}