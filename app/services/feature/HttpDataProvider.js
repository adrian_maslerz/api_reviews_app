const axios = require('axios');

module.exports = class
{
    constructor()
    {
        this.url = "https://www.x-kom.pl";
        this.http = axios;
    }

    search(search, page = 1)
    {
        return new Promise(resolve => {
            this.http.get(this.url + "/szukaj", {
                params: {
                    q: search.replace(/\s/g,"_"),
                    page: page,
                    per_page: 90
                },

            },{
                timeout: 5000
            })
            .then(response => resolve(response.data))
            .catch(error => resolve(""));
        });
    }

    getProduct(link)
    {
        return new Promise(resolve => {
            this.http.get(this.url + link, {},{
                timeout: 5000
            })
            .then(response => resolve(response.data))
            .catch(error => resolve(""));
        });
    }
}