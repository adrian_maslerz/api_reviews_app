const cheerio = require('cheerio');

module.exports = class
{
    loadData(data)
    {
        this.$ = cheerio.load(data);
    }

    //for products list
    getRatedProductsLinks()
    {
        const links = [];
        const $ = this.$;

        $("#productList .product-item a.rating-bar span.count").each(function() {
            links.push({
                link: $(this).parent().attr("href").replace(/#reviews/,""),
                image_link: $("img.img", $(this).parents(".product-item")).attr("src")
            });
        });

        return links;
    }

    getPagesNumber()
    {
        const $ = this.$;
        let pages = $("input#pageNumber + span").first().text();
        return parseInt(pages)
    }

    //for product details
    getProductInfo()
    {
        const $ = this.$;
        const specification = [];
        $(".sc-1ogetub-2.enJAZP > div").each(function()
        {
            specification.push({
                property: $(".kAqCCK", $(this)).text(),
                values: $(".isOIie", $(this)).map(function ()
                {
                    return $(this).text();
                }).get()
            });
        });

        const result = {
            title: $("h1").text(),
            specification: specification
        };

        return result;
    }

    getProductReviews()
    {
        const $ = this.$;
        const reviews = [];

        $(".sc-bZQynM.dCNFlc").each(function()
        {
            reviews.push({
                user: $(".umt56i-0.ikkAoU", $(this)).text(),
                date: $(".umt56i-5.jJHehS", $(this)).text(),
                description: $(".umt56i-11.jmltur span", $(this)).text(),
                ups: $(".umt56i-13.jyKSiE", $(this)).first().text(),
                downs: $(".umt56i-13.jyKSiE", $(this)).last().text(),
                rate: $(".sc-854dbh-1.eZYdpr[src$='79cb8d2579d03e0a.svg']", $(this)).length
            })
        });

        return reviews;
    }
}