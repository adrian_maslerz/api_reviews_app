const DataProcessor = require("./DataProcessor");

module.exports = class ETLStateHandler
{
    constructor()
    {
        //state
        this.extract = false;
        this.transform = false;
        this.load = false;
        this.processing = false;

        //processor
        this.processor = new DataProcessor();
        this.io = null;
    }

    async extractData(search)
    {
        if(this.processing) //preventing parallel processes
            return false;

        //enabling processing
        this.processing = true;
        this.io.emit('message', { etl: this.getCurrentETLState() });

        //extraction
        console.log("Starting search for:", search);
        this.io.emit('message', { log: "Starting search for: " + search });
        await this.processor.getSearchProductsLinks(search, this.io);

        console.log("Found", this.processor.links.length, "products")
        this.io.emit('message', { log: "Found " + this.processor.links.length + " products" });

        console.log("Starting extraction for", this.processor.links.length, "products")
        this.io.emit('message', { log: "Starting extraction for " + this.processor.links.length + " products" });
        await this.processor.getProductsWithReviews(this.io);

        console.log("Extraction finished! Fetched", this.processor.items.length, "items");
        this.io.emit('message', { log: "Extraction finished! Fetched " + this.processor.items.length + " items" });

        //enabling extract and disabling transform and load
        this.extract = true;
        this.transform = false;
        this.load = false;

        //disabling processing
        this.processing = false;
        this.io.emit('message', { etl: this.getCurrentETLState() });
    }

    transformData()
    {
        if(this.processing || !this.extract) //preventing parallel processes and rejecting when extraction has not finished
            return false;

        //enabling processing
        this.processing = true;
        this.io.emit('message', { etl: this.getCurrentETLState() });

        //Transformation
        console.log("Starting transformation");
        this.io.emit('message', { log: "Starting transformation" });
        this.processor.transformItemsToModels();

        console.log("Transformation finished! Prepared", this.processor.products.length, "products and", this.processor.reviews.length, "reviews");
        this.io.emit('message', { log: "Transformation finished! Prepared " + this.processor.products.length + " products and " + this.processor.reviews.length + " reviews"});

        //enabling transform and disabling load
        this.transform = true;
        this.load = false;

        //disabling processing
        this.processing = false;
        this.io.emit('message', { etl: this.getCurrentETLState() });
    }

    async loadData()
    {
        if(this.processing || !this.transform) //preventing parallel processes and rejecting when extraction has not finished
            return false;

        //enabling processing
        this.processing = true;
        this.io.emit('message', { etl: this.getCurrentETLState() });

        //Loading
        console.log("Starting loading");
        this.io.emit('message', { log: "Starting loading" });

        const result = await this.processor.saveModels(this.io);
        console.log("Loading finished! Loaded", result.loadedProducts, "products and", result.loadedReviews, "reviews");
        this.io.emit('message', { log: "Loading finished! Loaded " + result.loadedProducts + " products and " + result.loadedReviews + " reviews"});

        //enabling load
        this.extract = false;
        this.transform = false;
        this.load = false;

        //disabling processing
        this.processing = false;
        this.io.emit('message', { etl: this.getCurrentETLState() });
    }

    registerSockets(io)
    {
        this.io = io;

        //registering events
        this.io.on("connection", (socket) => {
            socket.emit('message', { etl: this.getCurrentETLState() });
        })
    }

    getCurrentETLState()
    {
        return {
            extract: this.extract,
            transform: this.transform,
            load: this.load,
            inProgress: this.processing
        }
    }
}