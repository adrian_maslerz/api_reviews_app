//services
const HttpDataProvider = require("./HttpDataProvider");
const DataExtractor = require("./DataExtractor");
const FileHandler = require("./../core/FileHandler");

//models
const { Product } = require("./../../models/Product");
const { Review } = require("./../../models/Review");

module.exports = class
{
    constructor()
    {
        this.extractor = new DataExtractor();
        this.dataProvider = new HttpDataProvider();
        this.fileHandler = new FileHandler();

        this.page = 1;
        this.pages = 1;

        this.links = [];
        this.items = [];
        this.products = [];
        this.reviews = [];
    }

    //EXTRACTION
    async getSearchProductsLinks(search, io)
    {
        //clearing holding variables
        this.page = 1;
        this.pages = 1;
        this.links = [];

        while(this.page <= this.pages)
        {
            const html = await this.dataProvider.search(search, this.page);
            this.extractor.loadData(html);

            console.log("Fetched page", this.page, this.pages);
            io.emit('message', { log: "Fetched page: " + this.page + "/" + this.pages });

            if(this.pages == 1)
                this.pages = this.extractor.getPagesNumber();

            const links = this.extractor.getRatedProductsLinks().filter(link => !this.links.map(link => link.link).includes(link.link)); //new links without duplicates
            this.links = this.links.concat(links);
            console.log("Extracted page: ", this.page);
            io.emit('message', { log: "Extracted page: " + this.page + "/" + this.pages});

            this.page++;
        }
    }

    async getProductsWithReviews(io)
    {
        //clearing holding variables
        this.items = [];

        for(let i = 0; i < this.links.length; i++)
        {
            const html = await this.dataProvider.getProduct(this.links[i].link);
            console.log("Fetched product", (i + 1), this.links[i].link);
            io.emit('message', { log: "Fetched product: " + (i + 1) + "/" + this.links.length + " " + this.links[i].link});

            this.extractor.loadData(html);
            const item = {
                product: this.extractor.getProductInfo(),
                reviews: this.extractor.getProductReviews()
            };

            item.product["link"] = this.dataProvider.url + this.links[i].link;
            item.product["original_image"] = this.links[i].image_link;


            console.log("Extracted product", item.product.title, "reviews", item.reviews.length);
            io.emit('message', { log: "Extracted product: " + (i + 1) + "/" + this.links.length + " " + item.product.title + " reviews " + item.reviews.length});

            this.items.push(item);
        }
    }

    //TRANSFORMATION
    transformItemsToModels()
    {
        //clearing holding variables
        this.products = [];
        this.reviews = [];

        this.items.forEach(item => {

            //preparing product
            const product = new Product({
                name: item.product.title,
                link: item.product.link,
                original_image: item.product.original_image,
                attributes: item.product.specification.map(spec => {
                    return { name: spec.property, values: spec.values }
                })
            });

            this.products.push(product);

            //preparing reviews
            item.reviews.forEach(reviewData => {

                const review = new Review({
                    product: product._id,
                    user: {
                        name: reviewData.user,
                    },
                    description: reviewData.description,
                    ups: reviewData.ups,
                    downs: reviewData.downs,
                    rate: reviewData.rate,
                    date: reviewData.date.replace(/(\s\|\s)/g," ")
                });

                this.reviews.push(review);
            });
        });
    }

    //LOADING
    async saveModels(io)
    {
        let loadedProducts = 0;
        let loadedReviews = 0;

        //saving products
        await Promise.all(this.products.map(product => {

            return new Promise(async resolve => {

                //checking if already loaded
                const existingProduct = await Product.findOne({ link: product.link }).exec();

                if(!existingProduct)
                {
                    const filePath = await this.fileHandler.saveExternalSourceFile(product.original_image, "external").catch(error => console.log(error));
                    console.log("Downloading image for", product.name);
                    io.emit('message', { log: "Downloading image for: " + product.name });

                    product.image = filePath || null;
                    await product.save().then(() => loadedProducts++).catch(error => console.log(error));
                }
                else
                {
                    this.reviews
                        .filter(review => review.product.toString() == product._id.toString())
                        .forEach(review => (review.product = existingProduct._id)) //replacing product id to existing one
                }

                resolve();
            });
        }));

        //saving reviews
        await Promise.all(this.reviews.map(review => {

            return new Promise(async resolve => {

                //checking if already loaded
                if(review.date)
                {
                    const reviewExists = await Review.count({ product: review.product, date: review.date.getTime() }).exec();
                    if(!reviewExists)
                        await review.save().then(() => loadedReviews++).catch(error => console.log(error));
                }

                resolve();
            })
        }));

        return { loadedProducts, loadedReviews }
    }
}