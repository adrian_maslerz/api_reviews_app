//core
const multer  = require('multer');
const fs = require('fs');
const axios = require("axios");
const mkdirp = require('mkdirp');

//custom
const Utils = require("./../utilities");
const variables = require('./../../../config/variables');

module.exports = class
{
    constructor(req, res)
    {
        //main
        this.req = req;
        this.res = res;

        if(req)
            this.host = (req.secure || req.headers["x-forwarded-proto"] ? "https://" : "http://") + req.headers.host;

        //file
        this.filePath = null;

        //uploads
        this.uploadsFolder = variables.folders.uploads;
        this.uploadsPath = variables.paths.folders.uploads;
    }

    getFileUrl(filePath)
    {
        return /^http/.test(filePath) ? filePath : this.host + this.uploadsPath + "/" + filePath;
    }

    handleSingleUpload(field, path, settings = {})
    {
        let { upload, constrains } = getUploader(field, path, settings);
        upload = upload.single(field);

        //registers on response hook to clear upload from failed actions
        this.res.on("finish", () => {
            if(this.res.statusCode != 200 && this.res.statusCode != 201)
                this.clearUpload();

            //removing given file if this is replacement method
            if(this.req.file && constrains.fileToRemove && (this.res.statusCode == 200 || this.res.statusCode == 201))
                deleteFile(constrains.fileToRemove);
        });

        return new Promise(resolve => {
            upload(this.req, this.res, (error) => {

                    if(error)
                    {
                        if(error.code == "LIMIT_FILE_SIZE")
                            return this.res.status(406).json(Utils.parseStringError(`Uploaded file can\'t be bigger than ${constrains.maxFileSize}MB.`, error.field ));

                        return this.res.status(406).json(Utils.parseStringError(error.message, error.field ));
                    }

                    resolve(this.filePath = this.req.file ? path + "/" + this.req.file.filename : undefined);
                }
            );
        });
    }

    handleMultiFilesUpload (field, path, settings = {})
    {
        let { upload, constrains } = getUploader(field, path, settings);
        upload = upload.array(field, constrains.maxFiles);

        //registers on response hook to clear upload from failed actions
        this.res.on("finish", () => {
            if(this.res.statusCode != 200 && this.res.statusCode != 201)
                this.clearUpload();;
        });

        return new  Promise((resolve) => {
            upload(this.req, this.res, (error) => {

                    if(error)
                    {
                        if(error.code == "LIMIT_FILE_SIZE")
                            return this.res.status(406).json(Utils.parseStringError(`Uploaded file can\'t be bigger than ${constrains.maxFileSize}MB.`, error.field ));

                        return this.res.status(406).json(Utils.parseStringError(error.message, error.field ));
                    }

                    const paths = this.req.files ? this.req.files.map(file => path + "/" + file.filename) : [];
                    resolve(paths);
                }
            );
        });
    }

    async saveExternalSourceFile(url, path)
    {
        if(!url)
            return resolve(null);

        //ensure folder path
        const folderPath = variables.folders.uploads + "/" + path;
        mkdirp.sync(folderPath);

        const returnPath = path + "/" + new Date().getTime() + (Math.random() * Math.random()) + url.slice(url.lastIndexOf("."));
        const fullPath = variables.folders.uploads + "/" + returnPath;

        const response = await axios({
            method: 'GET',
            url: url,
            responseType: 'stream'
        });

        response.data.pipe(fs.createWriteStream(fullPath));

        return new Promise((resolve, reject) => {
            response.data.on('end', () => {
                resolve(returnPath)
            })

            response.data.on('error', () => {
                reject()
            })
        });
    }

    static deleteFile (filePath)
    {
        deleteFile(filePath);
    }

    clearUpload()
    {
        //clears upload on unsuccessful
        if(this.req.file)
            fs.unlink(this.req.file.path, (error) => (error)? console.log(error) : removeEmptyDirectories(this.req.file.path));

        //clears multi upload on unsuccessful
        if(this.req.files)
        {
            this.req.files.forEach(file => {
                fs.unlink(file.path, (error) => (error)? console.log(error) : removeEmptyDirectories(file.path));
            });
        }
    }
}

function getUploader(field, path, settings = {})
{
    const constrains = {
        maxFileSize: settings.maxFileSize || 5,
        allowedMimeTypes: settings.allowedMimeTypes || [],
        fileToRemove: settings.fileToRemove || false,
        skipCondition: settings.skipCondition || (() => false),
        maxFiles: settings.maxFiles || 5
    };

    const storage = multer.diskStorage({
        destination: variables.folders.uploads + "/" + path + "/",
        filename: (req, file, cb) => cb(null, new Date().getTime() + file.originalname.replace(/\s/g, '-'))
    });

    const upload = multer({
        storage: storage,
        limits: {
            fileSize: 1024 * 1024 * constrains.maxFileSize
        },
        fileFilter: (req, file, cb) => {

            if(constrains.skipCondition())
                return cb(null, false);
            else
            {
                const valid = constrains.allowedMimeTypes.indexOf(file.mimetype) != -1;
                return valid ? cb(null, true) : cb({ message: (field.charAt(0).toUpperCase() + field.slice(1).toLowerCase()).replace(/_/g," ") + " extension is incorrect. Allowed extensions: " + constrains.allowedMimeTypes.join(', ') + ".", field: field });
            }
        }
    })

    return { upload, constrains }
}

function removeEmptyDirectories(path)
{
    const currentFolderPath = path.substr(0, path.replace(/\\/g,"/").lastIndexOf("/"));
    const fsSystem = fs;

    fsSystem.readdir(currentFolderPath, (error, files) => {
        if(!error && files && !files.length && currentFolderPath)
        {
            console.log("Removing", currentFolderPath)
            fsSystem.rmdir(currentFolderPath, (error) => {
                if(!error)
                    removeEmptyDirectories(currentFolderPath);
            });
        }
    });
}

function deleteFile (filePath)
{
    if(filePath)
    {
        fs.unlink(variables.folders.uploads + "/" + filePath, (error) => (error) ? console.log(error) : removeEmptyDirectories(variables.folders.uploads + "/" + filePath))
    }
}