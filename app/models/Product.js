//modules
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

//custom
const validators = require('./../services/validators');

//schema
const schema = mongoose.Schema({
	name: {
		type: String,
		required: [ true, "{PATH} field is required." ],
        trim: true
	},
	image: {
		type: String,
		default: null,
	},
    original_image: {
        type: String,
        default: null,
    },
    link: {
        type: String,
        required: [ true, "{PATH} field is required." ],
		validate: validators.url,
        trim: true
    },
	attributes: [
		{
			name: {
				type: String,
				required: [ true, "{PATH} field is required." ]
			},
			values: [
				{
                    type: String,
                    required: [ true, "{PATH} field is required." ]
				}
			]
		}
	],
    reviews: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Review",
        }
	],
	created: {
		type: Date,
		default: Date.now()
	}
}, { usePushEach: true });

//middlewares
schema.pre("save", async function (next)
{
    //dates handle
	const product = this;
    if(product.isNew)
        product.created = new Date();

    next();
});


//statics
schema.statics.parse = function(product, req)
{
    if(product.image)
    {
        const FileHandler = require("../services/core/FileHandler");
        const handler = new FileHandler(req);
        product.image = handler.getFileUrl(product.image);
    }

    if(product.created)
        product.created = product.created.getTime();

    return product;
}


schema.plugin(uniqueValidator, { message: 'The {PATH} has already been taken.' });
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Product: mongoose.model("Product", schema) }