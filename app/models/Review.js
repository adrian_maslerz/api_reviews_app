//modules
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

//schema
const schema = mongoose.Schema({
	product: {
        type: mongoose.Schema.Types.ObjectId,
        required: [ true, "{PATH} field is required." ],
        ref: "Product"
    },
	user: {
		name: {
            type: String,
            required: [ true, "{PATH} field is required." ],
            trim: true
		}
	},
    description: {
        type: String,
        required: [ true, "{PATH} field is required." ],
        trim: true
    },
    ups: {
        type: Number,
        required: [ true, "{PATH} field is required." ],
        min: [0, "{PATH} cannot be lower than {MIN}."]
    },
    downs: {
        type: Number,
        required: [ true, "{PATH} field is required." ],
        min: [0, "{PATH} cannot be lower than {MIN}."]
    },
    rate: {
        type: Number,
        required: [ true, "{PATH} field is required." ],
        min: [0, "{PATH} cannot be lower than {MIN}."],
        max: [6, "{PATH} cannot be greater than {MAX}."],
    },
    date: {
        type: Date,
        required: [ true, "{PATH} field is required." ]
    },
	created: {
		type: Date,
		default: Date.now()
	}
}, { usePushEach: true });

//middlewares
schema.pre("save", async function (next)
{
    //dates handle
	const review = this;
    if(review.isNew)
        review.created = new Date();

    //saving reference
    if(review.isNew)
    {
        const { Product } = require("./Product");
        const product = await Product.findById(review.product).catch(error => console.log(error));
        if(product)
        {
            product.reviews.push(review);
            await product.save().catch(error => console.log(error));
        }
    }

    next();
});


//statics
schema.statics.parse = function(review, req)
{
    if(review.created)
        review.created = review.created.getTime();

    if(review.date)
        review.date = review.date.getTime();

    return review;
}


schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Review: mongoose.model("Review", schema) }